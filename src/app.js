import React, { Component } from 'react';
import { Button } from './components/ui';
import { BasicCart, NikeCart } from './components/carts';

import './app.css';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = { client: 'basic' };

    this.onClick = this.onClick.bind(this);
  }

  onClick(e) {
    e.preventDefault();

    if (this.state.client === 'basic') {
      return this.setState({ client: 'nike'});
    }

    return this.setState({ client: 'basic' });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Client Carts</h2>
        </div>
        <Button onClick={ this.onClick } />
        {
          (this.state.client === 'basic') ?
            <BasicCart /> :
            <NikeCart />
        }
      </div>
    );
  }
}

export default App;
