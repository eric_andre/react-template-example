import React, { Component } from 'react';

class Button extends Component {
  render() {
    return (
      <button
        onClick={ this.props.onClick }
      >
        Change Client
      </button>
    );
  }
}

export default Button;
