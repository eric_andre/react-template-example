import React from 'react';

const products = [
  { name: 'shoes', price: '40.00' },
  { name: 'shirt', price: '12.00' },
  { name: 'pants', price: '25.00' },
  { name: 'hat', price: '15.00' }
];

class ProductList extends React.Component {
  render() {
    const productList = products.map((product, i) => {
      return (
        <li key={ i }>${ product.price }: { product.name }</li>
      );
    });

    return (
      <ul id="product-list">
        { productList }
      </ul>
    );
  }
}

export default ProductList;
