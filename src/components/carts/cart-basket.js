import React from 'react';

class CartBasket extends React.Component {
  render() {
    return (
      <span>Items in cart { this.props.count }</span>
    );
  }
}

export default CartBasket;
