import React, { Component } from 'react';
import CartBasket from './cart-basket';
import ProductList from './product-list';
import CheckoutButton from './checkout-button';

class BasicCart extends Component {
  render() {
    return (
      <div id="cart">
        <CartBasket count={ 4 } />
        <ProductList />
        <CheckoutButton />
      </div>
    );
  }
}

export default BasicCart;
