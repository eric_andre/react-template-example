import React, { Component } from 'react';
import CartBasket from './cart-basket';
import CheckoutButton from './checkout-button';
import PaypalButton from './paypal-button';

class NikeCart extends Component {
  render() {
    return (
      <div id="cart">
        <CartBasket count={ 2 } />
        <p> Thanks for shopping </p>
        <CheckoutButton />
        <p> Or checkout with Paypal</p>
        <PaypalButton />
      </div>
    );
  }
}

export default NikeCart;
