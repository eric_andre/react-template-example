import BasicCart from './basic-cart';
import NikeCart from './nike-cart';

export {
  BasicCart,
  NikeCart
};

export default {
  BasicCart,
  NikeCart
};
